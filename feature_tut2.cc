#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <set>
#include <stdio.h>
#include <stdlib.h>
#include "match_line.h"
#include "svg_writer.h"
#include "Utilities.h"
#include <ocradlib.h>

using namespace cv;
using namespace std;

/// Global variables
Mat src, src_gray, src_gray1, src_gray_blur;

int maxCorners = 75;
int maxTrackbar = 1000;

int lowThreshold = 50;
int const max_lowThreshold = 100;
int ratio = 3;
int kernel_size = 7;

RNG rng(12345);
const char* source_window = "Image";

/// Function header
void goodFeaturesToTrack_Demo( int, void* );



/**
 * @function main
 */
int main( int argc, char** argv )
{
  /// Load source image and convert it to gray
  src = imread( argv[1], 1 );
  Mat bgr[3];
  split(src, bgr);
  Mat& green_channel = bgr[1];

  cvtColor(src, src_gray, CV_BGR2GRAY);

  //Canny(src_gray1, src_gray, lowThreshold, lowThreshold*ratio, kernel_size);
  //bitwise_not ( src_gray, src_gray );

  //Mat& tmp(25)

  GaussianBlur(src_gray, src_gray_blur, Size(35, 35), 0, 0);
  //threshold(src_gray_blur, src_gray_blur, 230, 255, THRESH_BINARY);
  adaptiveThreshold(src_gray_blur, src_gray_blur, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 31, 1);

  /// Create Window
  namedWindow(source_window, CV_WINDOW_AUTOSIZE);
  namedWindow("test", CV_WINDOW_AUTOSIZE);

  /// Create Trackbar to set the number of corners
  createTrackbar("Max corners:", source_window, &maxCorners, maxTrackbar, goodFeaturesToTrack_Demo );

  imshow(source_window, src);
  imshow("test", src_gray_blur);

  goodFeaturesToTrack_Demo( 0, 0 );

  waitKey(0);
  return(0);
}

struct Cmp
{
   Cmp(Point ix): x(ix) {}
   bool operator() (Point a, Point b)
   {
      return cv::norm(x-a) <= cv::norm(x-b);
   }
   Point x;
};


string recogine_text(Rect rect, Mat& img)
{
   std::cout << "RECT" << rect  << std::endl;
   string result;
   uint8_t* pixels = (uint8_t*)malloc(rect.width*rect.height*sizeof(char));
   OCRAD_Pixmap pixmap;
   pixmap.height = rect.height;
   pixmap.width  = rect.width;
   pixmap.mode = OCRAD_greymap;
   pixmap.data = pixels;
   for (int j = 0; j < rect.height; j++)
   {
      for (int i = 0; i < rect.width; i++)
      {
         pixels[i+j*rect.width] = img.at<uint8_t>(rect.y+j, rect.x+i);
      }
   }
   struct OCRAD_Descriptor* desc = OCRAD_open();
   int ret1 = OCRAD_set_image(desc, &pixmap, false);
   int ret2 = OCRAD_set_threshold(desc, 128);
   int ret3 = OCRAD_set_utf8_format(desc, 0);
   int ret4 = OCRAD_recognize(desc, false);
   for (int i = 0; i < OCRAD_result_blocks(desc); i++)
   {
      cout << "Line:" << OCRAD_result_line(desc, i, 0) << endl;
      result = OCRAD_result_line(desc, 0, 0);
   }
   OCRAD_close(desc);
   free(pixels);
   return result;
}


/**
 * @function goodFeaturesToTrack_Demo.cpp
 * @brief Apply Shi-Tomasi corner detector
 */
void goodFeaturesToTrack_Demo( int, void* )
{
   if (maxCorners < 1) { maxCorners = 1; }

   /// Parameters for Shi-Tomasi algorithm
   vector<Point> corners;
   double qualityLevel = 0.01;
   double minDistance = 10;
   int blockSize = 3;
   bool useHarrisDetector = false;
   double k = 0.04;

   /// Copy the source image
   Mat copy;
   copy = src.clone();

   /// Apply corner detection
   goodFeaturesToTrack(src_gray,
         corners,
         maxCorners,
         qualityLevel,
         minDistance,
         Mat(),
         blockSize,
         useHarrisDetector,
         k );
   std::cout << "MAT:" << (int)src_gray.at<uint8_t>(101,41) << std::endl;
   std::set<int> visited;
   Cmp cmp(Point(0, 0));
   std::sort(corners.begin(), corners.end(), cmp);

   // Draw corners detected
   std::cout<<"Number of corners detected: " << corners.size()<<endl;
   int r = 4;
   for( int i = 0; i < corners.size(); i++ )
   {
      circle(copy,
            corners[i], r,
            Scalar(rng.uniform(0,255), rng.uniform(0,255),
               rng.uniform(0,255)),
            -1, 8, 0 );
   }
   namedWindow(source_window, CV_WINDOW_AUTOSIZE);
   imshow(source_window, copy);
   waitKey(0);

   std::vector<Line> all_lines;
   std::vector<Line> filt_lines;
   vector<Rect> rects;
   vector<Text> texts;

   for(int j = 0; j < corners.size(); j++)
   {
      if (visited.count(j))
         continue;

      std::vector<Line> shape;
      std::vector<int> idx_for_shape_to_visit;
      idx_for_shape_to_visit.push_back(j);

      for (auto idx = idx_for_shape_to_visit.begin(); idx != idx_for_shape_to_visit.end(); idx++)
      {
         std::cout << "idx " << *idx  << std::endl;

         // Check the current point connections
         for (int i = *idx+1; i < corners.size(); i++)
         {
            if (visited.count(i))
               continue;

            if (match_line(corners[*idx], corners[i], src_gray_blur))
            {
               const size_t diff = idx-idx_for_shape_to_visit.begin();
               idx_for_shape_to_visit.push_back(i);
               idx = idx_for_shape_to_visit.begin()+diff;
               Line new_line(corners[*idx], corners[i]);

               bool push = true;
               for (auto it = shape.begin(); it != shape.end(); it++)
               {
                  if (is_continuation(*it, new_line))
                  {
                     it->second = new_line.second;
                     push = false;
                     continue;
                  }
                  if (is_included(*it, new_line))
                  {
                     it->second = new_line.second;
                     push = false;
                     continue;
                  }
                  if (is_included2(*it, new_line))
                  {
                     push = false;
                     continue;
                  }
               }
               if (push)
               {
                  shape.push_back(new_line);
               }
            }
         }
         visited.insert(*idx);
      }

      Scalar color(rng.uniform(0,255), rng.uniform(0,255), rng.uniform(0,255));
      std::cout << "Found a shape of " << shape.size() << " " << color << std::endl;
      int noOfRightAng = 0;
      for (auto it = shape.begin(); it != shape.end(); it++)
      {
         std::cout << "Line " << it->first << "--" << it->second << std::endl;
         for (auto prev_line = shape.begin(); prev_line != it; prev_line++)
         {
            if (
                  ((prev_line->second == it->first) and isRightAng(prev_line->second, prev_line->first, it->second))
                  or ((prev_line->first == it->first) and isRightAng(prev_line->first, prev_line->second, it->second))
                  or ((prev_line->second == it->second) and isRightAng(prev_line->second, prev_line->first, it->first))
               )
            {
               std::cout << "RA" << std::endl;
               noOfRightAng++;
            }
         }
         line(copy, it->first, it->second, color, 3, 8, 0);
      }
      if (noOfRightAng >=3)
      {
         // assume rect...
         Point top_left = shape.begin()->first;
         Point bottom_right = shape.begin()->first;
         for (auto line = shape.begin(); line != shape.end(); line++)
         {
            if (cv::norm(line->first) <= cv::norm(top_left))
               top_left = line->first;
            if (cv::norm(line->second) <= cv::norm(top_left))
               top_left = line->second;

            if (cv::norm(line->first) >= cv::norm(bottom_right))
               bottom_right = line->first;
            if (cv::norm(line->second) >= cv::norm(bottom_right))
               bottom_right = line->second;
         }
         rects.push_back(Rect(top_left.x, top_left.y, bottom_right.x - top_left.x, bottom_right.y - top_left.y));
      } else {
         for (auto line = shape.begin(); line != shape.end(); line++)
         {
            all_lines.push_back(*line);
         }
      }
      namedWindow(source_window, CV_WINDOW_AUTOSIZE);
      imshow(source_window, copy);
      waitKey(0);
   }
      for(auto line : all_lines) {
	      bool push = true;
	      for(auto rect : rects) {
		      if(is_inside_rect(rect, line)) {
			      push = false;
			      break;
		      }
	      }
	      if(push) {
		      filt_lines.push_back(line);
	      }
      }

      std::cout << "got here" << std::endl;
      vector<Vec4i> filt_lines_c;
      for(auto l : filt_lines) {
	      Vec4i tmp(l.first.x, l.first.y, l.second.x, l.second.y);
	      filt_lines_c.push_back(tmp);
      }

      vector<int> labels;
      int num_groups = partition(filt_lines_c, labels, isInGroup);

	std::cout << num_groups << std::endl;
      vector<Line> final_lines;

      std::cout << "got here2" << std::endl;
      for(int group = 0; group < num_groups; group++) {
	      std::vector<int>::iterator it_index = find(labels.begin(), labels.end(), group);

	      Vec4i result = filt_lines_c[it_index-labels.begin()];
	      //it_index++;
	      while(it_index++ != labels.end()) {
		      it_index = std::find(it_index, labels.end(), group);
	      		Vec4i tmp = filt_lines_c[it_index-labels.begin()];
		      Vec4i result = combine2lines(result, tmp);
	      }
	      final_lines.push_back(Line(Point(result[0], result[1]), Point(result[2], result[3])));
      }
      std::cout << "got here3" << std::endl;

   //write_svg("test_shape1.svg", final_lines, rects);
   // try to detect text
   for (auto rect = rects.begin(); rect != rects.end(); rect++)
   {
      string s = recogine_text(*rect, src_gray);
      texts.push_back(Text(Point(rect->x, rect->y), s));
   }

   write_svg("test_shape1.svg", final_lines, rects, texts);

   /// Show what you got
   namedWindow(source_window, CV_WINDOW_AUTOSIZE);
   imshow(source_window, copy);
}

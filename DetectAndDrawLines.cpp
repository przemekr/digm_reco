
#include "Utilities.h"

Mat src;
Mat resultMat;
vector<Vec4i> lines;		// lines detected by Hough Transform
vector<Vec4i> uniqueLines;	// filtered lines
const char* lines_window = "Lines image";
const char* result_window = "Rsult image";
int lowThreshold = 50;
int const max_lowThreshold = 100;


/**
 * @function detectLines
 * @brief Trackbar callback
 */
static void detectLines(int, void*)
{
	
	Mat dst, cdst;
	uniqueLines.clear();
	
	/// Create a matrix of the same type and size as src (for result)
	resultMat.create( src.size(), src.type() );
	
	int kernel_size = 3;
	Canny(src, dst, 50, 50*3, kernel_size);
	cvtColor(dst, cdst, CV_GRAY2BGR);

	//vector<Vec4i> lines;
	//double rho =  ((double)lowThreshold)/max_lowThreshold;
	//std::cout << "lowThreshold=" << lowThreshold << std::endl;
	//std::cout << "rho=" << rho << std::endl;
	//if (rho <= 0) rho = 0.01;
	double rho = 1;
	double  	theta = CV_PI/180;
	int 		threshold;//= 68;
	lowThreshold <= 0 ? threshold = 1 : threshold = lowThreshold;
 	double  	minLineLength = 0;
	double  	maxLineGap = 50;
	HoughLinesP(dst, lines, rho, theta, threshold, minLineLength, maxLineGap );
	draw(cdst, lines);
	
	// display
	imshow(lines_window, cdst);


	// cluster lines that are close to each other
	std::vector<int> labels;
	int numberOfGroups = cv::partition(lines, labels, isInGroup2);
	std::cout << "------------numberOfGroups = " << numberOfGroups << std::endl;
	
	for (int group = 0; group < labels.size(); ++group) {
		auto it_found = std::find(labels.begin(), labels.end(), group); // it_found should not be nullptr. This is the first instance of this group
 		int index_found = it_found - labels.begin();
		Vec4i resultLine = lines[index_found];
		
		//  looking for the next instance
		while (it_found != labels.end()) {
			it_found = std::find(it_found+1, labels.end(), group); // it_found will change, either to the new found or to end()
			index_found = it_found - labels.begin();
			resultLine = combine2lines(resultLine,  lines[index_found]);
		}		
		uniqueLines.push_back(resultLine);
	}
	
	// close gaps in uniqueLines set
	for (auto l1 : uniqueLines) {
		for (auto l2 : uniqueLines) {
			if (l1 != l2 and isPerpendicular(l1,l2) ) {
				//std::cout << "AAAAAAAAAA" << std::endl;
				closeGap(l1,l2, 20);
			}
		}
	}
	
	draw(resultMat, uniqueLines);	
	imshow(result_window, resultMat);
	waitKey(5); // wait 5ms to redraw


}



/**
 * @function main
 */
int main( int, char** argv )
{
	//![load]
	src = imread( argv[1], IMREAD_COLOR ); // Load an image
//	cvtColor( src, src_gray, CV_BGR2GRAY );

	if( src.empty() )
	{ return -1; }
	//![load]


	// display the result

	const char* source_window = "Source image";
	namedWindow( source_window, CV_WINDOW_AUTOSIZE );
	imshow( source_window, src );


	namedWindow( lines_window, CV_WINDOW_AUTOSIZE );

	/// Create a Trackbar for user to enter threshold
	createTrackbar( "Min Threshold:", lines_window, &lowThreshold, max_lowThreshold, detectLines );

	/// detect lines, output to lines varibale	
	detectLines(0, 0); // the output is stored in global variable "lines"

	
	/// Wait until user exit program by pressing a key
	waitKey(0);

	return 0;

}

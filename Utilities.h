/*
Utilities
*/

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>

using namespace cv;

void drawLine(Mat& dst, Point& pt1, Point& pt2) {
	Scalar color = Scalar(0,0,255);
	line(dst, pt1, pt2, color/*, int thickness=1, int lineType=8, int shift=0*/);
}

void draw(Mat& dst, vector<Vec4i>& lines) {
	// clean up the dst	
	auto size = dst.size();
	auto type = dst.type();
	dst.release();
	dst.create(size, type);

	for( size_t i = 0; i < lines.size(); i++ ) {
		int thickness = 1;
		Vec4i l = lines[i];
		line(dst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), thickness, CV_AA);
		
	}
}

float distance(Point& p1, Point& p2) {
	return sqrtf((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y));
}

float length(const Vec4i& l1) {
	return sqrtf((l1[2] - l1[0])*(l1[2] - l1[0]) + (l1[3] - l1[1])*(l1[3] - l1[1]));
}

// inner product between two lines
float product(const Vec4i& l1, const Vec4i& l2) {
	return (l1[2] - l1[0])*(l2[2] - l2[0]) + (l1[3] - l1[1])*(l2[3] - l2[1]);
}

// get angle between two lines
float getAngle(const Vec4i& l1, const Vec4i& l2) {
	float cosAngle = fabs(product(l1,l2) / (length(l1)*length(l2)) );

	return acosf(cosAngle);
}

bool isPerpendicular(const Vec4i& l1, const Vec4i& l2) {
	
	float angle = getAngle(l1, l2);
	float a90 = CV_PI/2; // 90 degree
	float threshold = CV_PI/18; // 10 degree
	//std::cout << "a90 - threshold=" << a90 - threshold << std::endl;
	std::cout << "angle=" << getAngle(l1, l2) << std::endl;
	//std::cout << "a90 + threshold=" << a90 + threshold << std::endl;
	
	return (angle < (a90 + threshold) && angle > (a90 - threshold));

}

bool isParallel(const Vec4i& l1, const Vec4i& l2) {
	float angle = getAngle(l1, l2);
//std::cout << "-------------angle=" << angle << std::endl;
	float a180 = CV_PI; // 180 degree
	float threshold = CV_PI/18; // 10 degree
	if (angle < threshold || 
		( angle > (a180 - threshold)) ) {
		return true;
	} else return false; 
}

// if they are close to each other on the plane, and can be combined into 1 line
//bool isClose(const Vec4i& l1, const Vec4i& l2, float threshold) {
bool isClose(const Vec4i& l1, const Vec4i& l2) {

	// calculate the middle point of l1 and l2
	float mx1 = (l1[0] + l1[2]) * 0.5f;
	float mx2 = (l2[0] + l2[2]) * 0.5f;

	float my1 = (l1[1] + l1[3]) * 0.5f;
	float my2 = (l2[1] + l2[3]) * 0.5f;
	
	// calculate the distance between 2 midpoints
	float dist = sqrtf((mx1 - mx2)*(mx1 - mx2) + (my1 - my2)*(my1 - my2));

	if (dist > std::max(length(l1), length(l2)) * 0.5f)
		return false;

	return true;
}

// get the minimum gap between 2 points of each line
float getGap(const Vec4i& l1, const Vec4i& l2) {
	Point l1_p1 = Point(l1[0], l1[1]);
	Point l1_p2 = Point(l1[2], l1[3]);

	Point l2_p1 = Point(l2[0], l2[1]);
	Point l2_p2 = Point(l2[2], l2[3]);
	
	float min_gap = distance(l1_p1, l2_p1);
	float gap2 = distance(l1_p1, l2_p2);
	float gap3 = distance(l1_p2, l2_p1);
	float gap4 = distance(l1_p2, l2_p2);
	
	min_gap = std::min(min_gap, gap2);
	gap3 = std::min(gap3, gap4);
	min_gap = std::min(min_gap, gap3);
	
	return min_gap;
}

// using the gap between 2 lines to identify if they are close
bool isClose(const Vec4i& l1, const Vec4i& l2, float threshold) {
	return (getGap(l1,l2) <= threshold);
}

int max(int a, int b) {
	return (a < b ?  b : a);
}

bool isHorizontal(const Vec4i& l) {
	Vec4i horizontal = Vec4i(0,0,1,0);
	return isParallel(l, horizontal);	
}

bool isVertical(const Vec4i& l) {
	Vec4i vertical = Vec4i(0,0,0,1);
	return isParallel(l, vertical);	
}

Point rightMostPoint(const Point& p1, const Point& p2) {
	return (p2.x > p1.x ? p2 : p1);
}

Point leftMostPoint(const Point& p1, const Point& p2) {
	return (p2.x < p1.x ? p2 : p1);
}

Point highMostPoint(const Point& p1, const Point& p2) {
	return (p2.y > p1.y ? p2 : p1);
}

Point lowMostPoint(const Point& p1, const Point& p2) {
	return (p2.y < p1.y ? p2 : p1);
}

bool isInGroup(const Vec4i& l1, const Vec4i& l2) {
	return (isParallel(l1, l2) and isClose(l1, l2));
}

bool isInGroup2(const Vec4i& l1, const Vec4i& l2) {
	return (isParallel(l1, l2) and isClose(l1, l2, 20));
}

Vec4i combine2lines(const Vec4i& l1, const Vec4i& l2) {
	if (isParallel(l1, l2) ) {
		Point l1_p1 = Point(l1[0], l1[1]);
		Point l1_p2 = Point(l1[2], l1[3]);

		Point l2_p1 = Point(l2[0], l2[1]);
		Point l2_p2 = Point(l2[2], l2[3]);

		// if they are horizontal, take the rightMostPoint and leftMostPoint
		Point right_most_point;
		Point left_most_point;
		if (isHorizontal(l1) ) {
			right_most_point = rightMostPoint(rightMostPoint(l1_p1, l1_p2), rightMostPoint(l2_p1, l2_p2) );
			left_most_point = leftMostPoint(leftMostPoint(l1_p1, l1_p2), leftMostPoint(l2_p1, l2_p2) );
			// create a new line, connecting those 2 points
			return Vec4i(left_most_point.x, left_most_point.y, right_most_point.x, right_most_point.y);
		}

		// if they are vertical, take the highMostPoint and lowMostPoint
		Point high_most_point;
		Point low_most_point;
		if (isVertical(l1) ) {
			high_most_point = highMostPoint(highMostPoint(l1_p1, l1_p2), highMostPoint(l2_p1, l2_p2) );
			low_most_point = lowMostPoint(lowMostPoint(l1_p1, l1_p2), lowMostPoint(l2_p1, l2_p2) );
			// create a new line, connecting those 2 points
			return Vec4i(low_most_point.x, low_most_point.y, high_most_point.x, high_most_point.y);
		}

		// if they are in the middle, both of the above will work
		high_most_point = highMostPoint(highMostPoint(l1_p1, l1_p2), highMostPoint(l2_p1, l2_p2) );
		low_most_point = lowMostPoint(lowMostPoint(l1_p1, l1_p2), lowMostPoint(l2_p1, l2_p2) );
		// create a new line, connecting those 2 points
		return Vec4i(low_most_point.x, low_most_point.y, high_most_point.x, high_most_point.y);
		
	}

}


Point getMidPoint(const Point& p1, const Point& p2) {
	int mx = (p1.x + p2.x) * 0.5;
	int my = (p1.y + p2.y) * 0.5;
	return Point(mx, my);
}

// seal line1::point (isFirst1 says if it is point1 or point2) with line2::point.
void seal(Vec4i& l1, Point& p1, bool isFirst1, Vec4i& l2, Point& p2, bool isFirst2) {
	std::cout << "seal" << std::endl;
	Point midPoint = getMidPoint(p1,p2);
	
	// change line1
	if (isFirst1) {
		l1[0] = midPoint.x;
		l1[1] = midPoint.y;
	} else {
		l1[2] = midPoint.x;
		l1[3] = midPoint.y;
	}
	
	// change line2
	if (isFirst2) {
		l2[0] = midPoint.x;
		l2[1] = midPoint.y;
	} else {
		l2[2] = midPoint.x;
		l2[3] = midPoint.y;
	}
	
	
}

void closeGap(Vec4i& l1, Vec4i& l2, int threshold) {
	std::cout << "CLOSE GAP" << std::endl;
	float min_gap = getGap(l1,l2);
	if (isPerpendicular(l1,l2) and ( min_gap < threshold)) {
		std::cout << "satisfy conditions" << std::endl;
		Point l1_p1 = Point(l1[0], l1[1]);
		Point l1_p2 = Point(l1[2], l1[3]);

		Point l2_p1 = Point(l2[0], l2[1]);
		Point l2_p2 = Point(l2[2], l2[3]);
		
		const float gap1 = distance(l1_p1, l2_p1);
		const float gap2 = distance(l1_p1, l2_p2);
		const float gap3 = distance(l1_p2, l2_p1);
		const float gap4 = distance(l1_p2, l2_p2);
		

		if (min_gap == gap1) {seal(l1, l1_p1, true, l2, l2_p1, true); return; }
		if (min_gap == gap2) {seal(l1, l1_p1, true, l2, l2_p2, false); return; }
		if (min_gap == gap3) {seal(l1, l1_p2, false, l2, l2_p1, true); return; }
		if (min_gap == gap4) {seal(l1, l1_p2, false, l2, l2_p2, false); return; }
	}
}







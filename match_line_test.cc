#include "match_line.h"
#include "gtest/gtest.h"

TEST(match_line_test, is_cont) {
   Line l(Point(10, 10), Point(10, 12));
   Line k(Point(10, 10), Point(11, 140));
   Line kp(Point(10, 10), Point(11, 141));
   Line m(Point(10, 12), Point(11, 140));
   Line n(Point(10, 12), Point(120, 140));
   Line o(Point(10, 10), Point(120, 140));

   ASSERT_TRUE(is_continuation(l, m));
   ASSERT_FALSE(is_continuation(l, n));
   ASSERT_FALSE(is_continuation(k, k));

   ASSERT_TRUE(is_included(l, k));
   ASSERT_FALSE(is_included(l, o));
   ASSERT_TRUE(is_included(k, kp));
};

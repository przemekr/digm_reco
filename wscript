#! /usr/bin/env python
APPNAME = 'DigmReco'
VERSION = '0.0001'
pkgname = '%s-%s'% (APPNAME.lower(), VERSION)
top = '.'
out = 'build'


from waflib import Configure, Logs
from waflib import TaskGen
from os import path
import subprocess

def options(opt):
    opt.load('compiler_cxx compiler_c waf_unit_test')


def configure(conf):
    conf.load('compiler_cxx compiler_c waf_unit_test')
    ##conf.check_cxx(lib='opencv',  uselib_store='cv')

    conf.define('APPNAME', APPNAME)
    conf.define('VERSION', VERSION)
    conf.check_cfg(package='opencv', uselib_store='cv', atleast_version='2.4',
                args='--cflags --libs')
    conf.env.append_unique('CXXFLAGS', ['-g', '-std=c++11'])
    conf.check_cxx(lib=['gtest', 'gtest_main'], header_name="gtest/gtest.h",
            mandatory=False,
            uselib_store='g',
            define="have_gtest")
    conf.check_cxx(lib='pthread',      uselib_store='l')
    conf.write_config_header('config.h')

def build(bld):
    bld.env.VERSION = VERSION
    bld.program(source='feature_tut1.cc', target="feature_tut1", use=['cv'])
    bld.program(source='CannyDetector.cc', target="CannyDetector", use=['cv'])
    bld.program(source='rotate.cc', target="rotate", use=['cv'])
    bld.program(source='DetectAndDrawLines.cpp', target="DrawLines", use=['cv'])
    bld.program(source='TestUtilities.cpp', target="test", use=['cv'])
    bld.program(source='objectDetection.cpp', target="facerec_video", use=['cv'])
    ###bld.program(source='TestUtilities.cpp', target="test", use=['cv'])
    #bld.recurse('src')

    bld.stlib(
            source   = bld.path.ant_glob('ocrad-0.23/*.cc', exclude='*main.cc'),
            target   = 'ocrad',
            name     = 'ocrad',
            defines  = 'PROGVERSION="0.23"',
            includes = 'ocrad-0.23'
            )

    bld.program(
            source=['feature_tut2.cc', 'match_line.cc'],
            target="feature_tut2", use=['cv', 'ocrad'],
            cxxflags = '-std=c++11 -ggdb',
            includes = 'ocrad-0.23'
            )

    if bld.env.LIB_g:
        bld.program(features='test', source=['match_line.cc', "match_line_test.cc"], target="match_line_test", use=['cv', 'g'])

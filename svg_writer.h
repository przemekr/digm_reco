#include "opencv2/imgproc/imgproc.hpp"
#include <sstream>
#include <fstream>

using namespace cv;
using namespace std;

typedef std::pair<Point, Point> Line;
struct Text {
   Point where;
   string what;
   Text(Point w, string t): where(w), what(t) {}
};

static int id;

string get_hex(uint8_t R, uint8_t G, uint8_t B) {
   char out[6];
   sprintf(out, "%02x%02x%02x", R, G, B);
   return string(out);
}

string svg_write_line(Line& line, uint8_t R, uint8_t G, uint8_t B)
{
   stringstream ss;
   ss << "<path " <<
      " style=\"stroke:#"<<get_hex(R,G,B)<<";stroke-width:3\" " 
      << " d=\"M " << line.first.x << "," << line.first.y
      << " "       << line.second.x << "," << line.second.y << "\""
      << " id=\"path" << id++ << "\"" << " inkscape:connector-curvature=\"0\" />\n";
   return ss.str();
}

string svg_write_txt(Text& t)
{
   stringstream ss;
   ss << "<text "
      << " style=\"font-style:normal;font-weight:normal;font-size:56.57142639px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\""
      << " x=\"" << t.where.x << "\""
      << " y=\"" << t.where.y << "\""
      << " id=\"text" << id++ << "\""
      << "> <tspan>" << t.what << "</tspan></text>\n";
      return ss.str();
}

string svg_write_rect(Rect& rect, uint8_t R, uint8_t G, uint8_t B) //color
{
   stringstream ss;
   ss << "<rect " <<
      " style=\"stroke:#"<< get_hex(R,G,B) <<";stroke-width:3;fill-opacity:0.1\" " 
      << " id=\"rect" << id++ << "\"" << " inkscape:connector-curvature=\"0\""
      << " width=\"" << rect.width << "\""
      << " height=\"" << rect.height << "\""
      << " x=\"" << rect.x << "\""
      << " y=\"" << rect.y << "\" />\n\n";
   return ss.str();
}

void write_svg(string path, vector<Line> lines, vector<Rect> rects, vector<Text> texts)
{
      std::ofstream file;
      file.open(path);
      file << "\
<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n\
<!-- Created with Inkscape (http://www.inkscape.org/) -->\n\
\n\
<svg\n\
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n\
   xmlns:cc=\"http://creativecommons.org/ns#\"\n\
   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n\
   xmlns:svg=\"http://www.w3.org/2000/svg\"\n\
   xmlns=\"http://www.w3.org/2000/svg\"\n\
   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\n\
   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\n\
   width=\"297mm\"\n\
   height=\"210mm\"\n\
   viewBox=\"0 0 744.09448819 1052.3622047\"\n\
   id=\"svg2\"\n\
   version=\"1.1\"\n\
   inkscape:version=\"0.91 r\"\n\
   sodipodi:docname=\"test.svg\">\n\
  <defs\n\
     id=\"defs4\" />\n\
  <sodipodi:namedview\n\
     id=\"base\"\n\
     pagecolor=\"#ffffff\"\n\
     bordercolor=\"#666666\"\n\
     borderopacity=\"1.0\"\n\
     inkscape:pageopacity=\"0.0\"\n\
     inkscape:pageshadow=\"2\"\n\
     inkscape:zoom=\"0.35\"\n\
     inkscape:cx=\"-47.857143\"\n\
     inkscape:cy=\"520\"\n\
     inkscape:document-units=\"px\"\n\
     inkscape:current-layer=\"layer1\"\n\
     showgrid=\"false\"\n\
     inkscape:window-width=\"768\"\n\
     inkscape:window-height=\"1024\"\n\
     inkscape:window-x=\"47\"\n\
     inkscape:window-y=\"24\"\n\
     inkscape:window-maximized=\"1\" />\n\
  <metadata\n\
     id=\"metadata7\">\n\
    <rdf:RDF>\n\
      <cc:Work\n\
         rdf:about=\"\">\n\
        <dc:format>image/svg+xml</dc:format>\n\
        <dc:type\n\
           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n\
        <dc:title></dc:title>\n\
      </cc:Work>\n\
    </rdf:RDF>\n\
  </metadata>\n\
  <g inkscape:label=\"Layer 1\" inkscape:groupmode=\"layer\" id=\"layer1\">\n";

      for (auto i = lines.begin(); i != lines.end(); i++)
      {
         file << svg_write_line(*i, 0, 0, 0);
      }
      for (auto i = rects.begin(); i != rects.end(); i++)
      {
         file << svg_write_rect(*i, 255, 0, 0);
      }
      for (auto i = texts.begin(); i != texts.end(); i++)
      {
         file << svg_write_txt(*i);
      }
      file << "\
         </g>\n\
    </svg>\n\
";
      file.close();
}

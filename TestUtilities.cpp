
#include "Utilities.h"



/**
 * @function main
 */
int main( int, char** argv )
{
	// test getAngle: float getAngle(const Vec4i& l1, const Vec4i& l2) 
	Vec4i l1(0,0,2,0);//horizontal
	Vec4i l2(1,1,2,2);//45 degree
	Vec4i l3(1,1,4,1); //horizontal
	Vec4i l4(1,1,1,2); //vertical
	Vec4i l5(0,0,2,0.5);//horizontal
	Vec4i l6(1,1,1.5,6); //vertical

	std::cout << "Angle45 =" << getAngle(l1,l2) << std::endl;
	std::cout << "Angle small =" << getAngle(l1,l5) << std::endl;
	std::cout << "Angle90 =" << getAngle(l1,l4) << std::endl;
	std::cout << "AngleAlmost90 =" << getAngle(l5,l6) << std::endl;

	// bool isParallel(const Vec4i& l1, const Vec4i& l2)	
	std::cout << "parellelTrue =" << isParallel(l1,l3) << std::endl;
	std::cout << "parellelFalse =" << isParallel(l4,l3) << std::endl;

	// Vec4i combine2lines(const Vec4i& l1, const Vec4i& l2)
	Vec4i result = combine2lines(l1,l3);
	std::cout << "resultLine = p1(" << result[0] <<","<<result[1]<< "), p2(" << result[2] <<","<<result[3] << ")"<< std::endl;

	return 0;
}

/*
Rotate an image with an angel clock-wise
*/

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>

using namespace cv;


/**
 * @function main
 */
int main( int, char** argv )
{
	//![load]
	Mat src = imread( argv[1], IMREAD_COLOR ); // Load an image
//	cvtColor( src, src_gray, CV_BGR2GRAY );

	if( src.empty() )
	{ return -1; }
	//![load]

	// Initialize the destination image as having the same size and type as the source:
	Mat rotate_dst; // = Mat::zeros( src.rows, src.cols, src.type() );
	rotate_dst.create( src.size(), src.type() );

	// define requirements to rotate
	Point center = Point( src.cols/2, src.rows/2 );// center of rotation
	double angle = -50.0; 	// rotate clock-wise with 50 degree
	double scale = 1.0; 	// scale factor, don't scale as set it to 1-0

	// get the rotation matrix
	Mat rot_mat = getRotationMatrix2D( center, angle, scale );

	//We now apply the found rotation to the src image
	warpAffine( src, rotate_dst, rot_mat, src.size() );

	// display the result

	const char* source_window = "Source image";
	namedWindow( source_window, CV_WINDOW_AUTOSIZE );
	imshow( source_window, src );

	const char* rotate_window = "Rotate image";
	namedWindow( rotate_window, CV_WINDOW_AUTOSIZE );
	imshow( rotate_window, rotate_dst );

	  /// Wait until user exit program by pressing a key
	waitKey(0);

	return 0;

}

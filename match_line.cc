#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "match_line.h"

using namespace cv;
using namespace std;
static RNG rng(12345);
bool match_line(Point a, Point b, Mat img)
{
   const double no_points = 100;
   const double thr = 184;
   const double no_of_match = 0.90;

   int score = 0;
   for (int i = 0; i < no_points; i++)
   {
      double dx = (b.x-a.x)/no_points;
      double dy = (b.y-a.y)/no_points;
      //Point mid = Point(a.x + i*dx+rng.gaussian(1), a.y+i*dy+rng.gaussian(1));
      Point mid = Point(a.x + i*dx, a.y+i*dy);
      score += (int)img.at<uint8_t>(mid) <= thr? 1:0;
   }
   return score > no_points*no_of_match;
}

double ang(Point a, Point b, Point c)
{
   Point ad = a - b;
   Point cd = a - c;
   return 180/M_PI*(atan2(ad.y, ad.x)-atan2(cd.y, cd.x));
   
}

bool is_continuation(Line& l, Line& k)
{
   if (l.second != k.first)
      return false;

   double angle = ang(l.first, k.first, k.second);
   return angle > -10 and angle < 10;
}

double cross_product(Point a, Point b)
{
	return a.x*b.y - a.y*b.x;
}

double distance_to_line( Point begin, Point end, Point x ){
   //translate the begin to the origin
   end -= begin;
   x -= begin;

   //¿do you see the triangle?
   double area = cross_product(x, end);
   return area / norm(end);
}

bool is_inside(Line& l, Line& k) {
   //double dist = 0.1 * norm(l.first, l.second);
   if (distance_to_line(l.first, l.second, k.first) < 10 and distance_to_line(l.first, l.second, k.second) < 10) {
      return true;
   }
   return false;
}

bool is_included(Line& l, Line& k)
{
   if (l.first != k.first)
   {
      return false;
   }
   if (l.second == k.second)
   {
      return true;
   }

   double angle = ang(l.second, k.first, k.second);
   return angle > 180-10 and angle < 180+10;
}

bool is_inside_rect(Rect& r, Line& l)
{
	if(l.first.x >= r.x and l.first.x <= (r.x+r.width) and
	   l.second.x >= r.x and l.second.x <= (r.x+r.width) and
	   l.first.y >= r.y and l.first.y <= (r.y+r.height) and
           l.second.y >= r.y and l.second.y <= (r.y+r.height)) {
		return true;
	}
	return false;
}

bool is_included2(Line& l, Line& k)
{
   if (l.second != k.second)
   {
      return false;
   }
   if (l.first == k.first)
   {
      return true;
   }

   double angle = ang(l.first, k.second, k.first);
   return angle > -10 and angle < +10;
}


bool isRightAng(Point a, Point b, Point c)
{
   double angle = ang(a, b, c);
   std::cout << "isRightAng:" << angle << "," << a << b << c << std::endl;
   return
      (angle > 80 and angle < 100)
      or 
      (angle > 260 and angle < 280)
      ;
}

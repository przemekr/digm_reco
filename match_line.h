#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

typedef std::pair<Point, Point> Line;
bool match_line(Point a, Point b, Mat img);
bool is_continuation(Line& l, Line& k);
bool is_included(Line& l, Line& k);
bool is_inside_rect(Rect& r, Line& l);
bool is_included2(Line& l, Line& k);
double ang(Point a, Point b, Point c);
bool isRightAng(Point a, Point b, Point c);
